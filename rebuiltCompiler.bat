﻿@echo off

cls

set openWhenFinish=%1

call CleanAll.bat noCheck

set pauseOpt=nopause

@echo off
call CheckInstall.bat CMAKE

if ErrorLevel == 1 (
    goto fail
)


SET bDirVar=projectSolution

echo Removing Old Build Directory

rd %bDirVar% /Q /S

mkdir %bDirVar%

PUSHD %bDirVar%

SET VS_INSTALLER_PATH=%ProgramFiles(x86)%\Microsoft Visual Studio\Installer
SET VS_INSTALLER_EXE=%VS_INSTALLER_PATH%\vs_installer.exe
SET VS_2019_INSTALL_PATH=""
SET VS_2019_EXE=""

PUSHD "%VS_INSTALLER_PATH%"
for /f "delims=" %%a in ('vswhere.exe -products Microsoft.VisualStudio.Product.Community -version 16 -nologo -property installationPath') do (
    SET VS_2019_INSTALL_PATH=%%a
)
for /f "delims=" %%a in ('vswhere.exe -products Microsoft.VisualStudio.Product.Community -version 16 -nologo -property productPath') do (
    SET VS_2019_EXE=%%a
)

SET VS_INSTALLER_PATH
SET VS_INSTALLER_EXE
SET VS_2019_INSTALL_PATH
SET VS_2019_EXE

::call "%VS_2019_INSTALL_PATH%\Common7\Tools\VsDevCmd.bat"
::SET CMAKE_GENERATOR_INSTANCE="%VS_2019_EXE%"

POPD

if exist "%CMAKE_PATH%/cmake.exe" (
"%CMAKE_PATH%/cmake" .. -G "Visual Studio 17 2022" -A x64
goto generated
)

cmake .. -G "Visual Studio 17 2022" -A x64

:generated

for /f "delims=" %%a in ('DIR /B *.sln') do (
    SET SOLUTION_NAME=%%a
)
SET SOLUTION_NAME

POPD

MKLINK %SOLUTION_NAME% .\%bDirVar%\%SOLUTION_NAME%

if NOT %pauseOpt% == nopause (
    pause
)

if "%openWhenFinish%"=="-o" (
	.\%bDirVar%\%SOLUTION_NAME%
)

goto end:

:fail
    echo.
    echo Rebuild FAILED
    goto end

:end