#include "stdafx.h"

#include "Resource.h"
#include "Engine/DispositifD3D11.h"
#include "GameState.h"

#include "Engine/MoteurModule.h"

#include "Skybox.h"

namespace PM3D {

   Skybox::Skybox(const Orientation& _orientation, DispositifD3D11* _pDispositif)
      :GameObject(_orientation)
   {
      Init(_pDispositif);
   }

   void Skybox::Init(DispositifD3D11 * _pDispositif)
   {
      orientation.setScale(DirectX::XMFLOAT3{ 2500.0f, 2500.0f, 2500.0f });
      skyboxMesh = MoteurWindows::GetInstance().GetRessourcesManager().GetModelMesh(ModelType::SKYBOX);
      skyboxMesh->shaderFilename = L"../ressources/shaders/Skybox.fx";
      skyboxMesh->InitSkybox(_pDispositif);
      //skyboxMesh->InitEffetSkybox();
   }

   void Skybox::Draw(ID3D11ShaderResourceView* pDepthShaderResourceView) {
       skyboxMesh->DrawSkybox(matWorld);
   }
   void Skybox::Update(const float tempsEcoule)
   {
      calculMatriceWorld();
   }

}

