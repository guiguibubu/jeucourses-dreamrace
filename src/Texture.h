#ifndef TEXTURE_H
#define TEXTURE_H

namespace PM3D
{
	class DispositifD3D11;

	class Texture
	{
	public:
		Texture(const std::wstring& filename, DispositifD3D11* pDispositif);
		~Texture();

		ID3D11ShaderResourceView* ressource;

		const std::wstring& GetFilename() const { return m_Filename; }

	private:
		std::wstring m_Filename;
	};

} // namespace PM3D
#endif