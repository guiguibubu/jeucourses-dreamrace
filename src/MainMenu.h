#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <memory>

namespace PM3D {

	class AfficheurSprite;
	class AfficheurTexte;
	class DispositifD3D11;

	using ZoneTextPtr = std::unique_ptr<AfficheurTexte>;

	class MainMenu
	{
		std::unique_ptr<Gdiplus::Font> pPolice;
		ZoneTextPtr text;

		float accumulatedTime{};
		bool toggleText = true;
	public:
		MainMenu() = default;
		void Init(DispositifD3D11*, std::unique_ptr<AfficheurSprite>&);
		~MainMenu() = default;

		void Update(const float);
	};

} // namespace PM3D


#endif