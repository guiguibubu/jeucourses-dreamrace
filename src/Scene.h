#ifndef SCENE_H
#define SCENE_H

#include <array>
#include <vector>


namespace PM3D {

    class GameObject;
    class LumiereManager;
    class Terrain;
    class Vehicule;
    class Skybox;
    class Item;
    class Zone;
    class Chateau;
    class DispositifD3D11;
    class FrustrumView;
    class RandomGenerateur;
    class ScenePhysique;

    enum GameState;

    using GameObjectPtr = std::unique_ptr<GameObject>;

    class Scene {
        static constexpr size_t NB_ZONES = 4;
        static constexpr size_t MAX_OBJETS = 300;
        //GameManager gameManager;
        LumiereManager lumiereManager;
        std::unique_ptr<Terrain> terrain;
        std::unique_ptr<Vehicule> vehicule;
        std::unique_ptr<Skybox> skybox;
        std::vector<std::unique_ptr<Item>> items;
        std::vector<GameObjectPtr> objects;
        int CompteurItemRecup = 0;
        std::array<Zone, NB_ZONES> zones;
        std::unique_ptr<Chateau> chateau;

    public:
        Scene();
        ~Scene() = default;

        void InitPhysX();
        void AddObject(GameObjectPtr&& objPtr);
        void InitObjets(DispositifD3D11*);
        void Draw(const FrustrumView& frustrumView, const GameState& state);
        void UpdateItems(float _deltaTime);
        void UpdateNbItems();
        void Anime(float);
        void Clear();
        RandomGenerateur* Rand;
        Terrain* getTerrain();
        ScenePhysique scenePhysique;
        Vehicule* getVehicule();
        Chateau& getChateau();
        LumiereManager& getLumiereManager();
        std::vector<std::unique_ptr<Item>>& getItems();
    private:
        void InitZones();
        void InitItems(DispositifD3D11 * _dispositif);
        void InitChateau(ScenePhysique& scenePhysique);
        void DrawItems();
        void DrawShadowItems();

        // Pour les ombres
        DispositifD3D11* pDispositif;		// On prend en note le dispositif

        const int SHADOWMAP_DIM = 2048;
        const float ANGLE_LUMIERE = DirectX::XM_PIDIV4 * 2.f / 3.f;
        void InitLumieres();
        void InitMatricesShadowMap();
        void UpdateLumiere();
         
        DirectX::XMVECTOR oldPosistionPlayer;

        XMMATRIX matViewLight;
        XMMATRIX matProjLight;
        XMMATRIX matViewProjLight;

     
        ID3D11Texture2D* pDepthTexture; // texture de profondeur 
        ID3D11DepthStencilView* pDepthStencilView;
        ID3D11ShaderResourceView* pDepthShaderResourceView;

        void InitBufferShadowMapping();
    };

} // namespace PM3D


#endif