#ifndef HUD_H
#define HUD_H

#include <memory>
#include <string>
#include <chrono>

namespace PM3D
 {
    class AfficheurTexte;
    class AfficheurSprite;
    class DispositifD3D11;

    using ZoneTextPtr = std::unique_ptr<AfficheurTexte>;
    using FontPtr = std::unique_ptr<Gdiplus::Font>;

    class HUD
    {
        const std::wstring SPEED_PREFIX = L"Vitesse: ";
        const std::wstring SPEED_SUFFIX = L" cm/s";

        ZoneTextPtr speedText;
        FontPtr pPoliceSpeed;

        ZoneTextPtr remainingTimeText;
        FontPtr pPoliceTime;

        ZoneTextPtr scoreText;
        ZoneTextPtr timeRemainingNightmare;

    public:
        HUD() = default;
        void Init(DispositifD3D11*, std::unique_ptr<AfficheurSprite>&);
        ~HUD() = default;

        void Update(const int speedValue, const std::chrono::milliseconds deltaTime, const std::chrono::milliseconds timeBeforeNightmare, std::pair<unsigned int, unsigned int> oursResult);
    };
}
#endif