#ifndef OPTIONS_MENU_H
#define OPTIONS_MENU_H

#include <memory>

namespace PM3D {
    
    class AfficheurSprite;
    class AfficheurTexte;
    class DispositifD3D11;
    class InputMenuComponent;

    using ZoneTextPtr = std::unique_ptr<AfficheurTexte>;

    enum Selectable { DIFFICULTY_EASY, DIFFICULTY_MEDIUM_SAIGNANT, DIFFICULTY_HARD, BACK_TO_GAME };

   class OptionsMenu
   {
       std::unique_ptr<Gdiplus::Font> pPoliceTitle;
       std::unique_ptr<Gdiplus::Font> pPolice;
       ZoneTextPtr title;
       ZoneTextPtr difficulty;
       ZoneTextPtr backToGame;
       ZoneTextPtr selector;
       AfficheurSprite* display;

       std::unique_ptr<InputMenuComponent> input;
 
   public:
       OptionsMenu() = default;
       void Init(DispositifD3D11*, std::unique_ptr<AfficheurSprite>&);
       ~OptionsMenu() = default;

       void Update(const float);
   };

} // namespace PM3D


#endif