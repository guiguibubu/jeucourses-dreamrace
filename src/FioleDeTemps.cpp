#include "stdafx.h"

#include "Util/util.h"
#include "Engine/DispositifD3D11.h"
#include "Resource.h"
#include "Util/toolsMath.h"
#include "GameState.h"

#include "Engine/MoteurModule.h"

#include "FioleDeTemps.h"

#include <chrono>
using namespace std;
using namespace std::chrono;

namespace PM3D
{
    FioleDeTemps::FioleDeTemps(const Orientation& _orientation, const float _dx, const float _dy, const float _dz, DispositifD3D11* _pDispositif, ScenePhysique& scenePhysique)
        : Item(_orientation, _dx, _dy, _dz, _pDispositif, scenePhysique, ModelType::SANDWATCH) {
		 physx::PxFilterData filterData;
		 filterData.word0 = ScenePhysique::Filter_Group::ITEM;
		 filterData.word1 = ScenePhysique::Filter_Group::TIME;
		 filterData.word3 = this->physic->shape->getSimulationFilterData().word3;
		 this->physic->shape->setSimulationFilterData(filterData);
    }

    void FioleDeTemps::OnTrigger() {
        GameManager& gameManager = MoteurWindows::GetInstance().getGameManager();
        gameManager.addTempsDeJeu(20'000ms);
    }

    bool FioleDeTemps::isCatchable() const noexcept {
        GameManager& gameManager = MoteurWindows::GetInstance().getGameManager();
        return gameManager.getMondeActuel() == GameManager::Monde::CAUCHEMAR;
    }
} // namespace PM3D
