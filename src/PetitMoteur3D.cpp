// PetitMoteur3D.cpp: definit le point d'entree pour l'application.
//

#include "stdafx.h"

#include "Engine/DispositifD3D11.h"
#include "GameState.h"

#include "Engine/MoteurModule.h"

#include "PetitMoteur3D.h"

#ifdef PM3D_WINDOWS

int APIENTRY _tWinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	// Pour ne pas avoir d'avertissement
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);

    // Creation de l'objet Moteur
    PM3D::MoteurWindows& rMoteur = PM3D::MoteurWindows::GetInstance();

    // Specifiques a une application Windows
    rMoteur.SetWindowsAppInstance(hInstance);

    // Initialisation du moteur
    rMoteur.Initialisations();

    // Boucle d'application
    rMoteur.Run();

    return (int)1;
}

#endif
