#pragma once

#pragma region Moteur

#include "Engine/dispositif.h"
#include "Engine/DispositifD3D11.h"

// Include independant
#include "Formes/Orientation.h"
#include "Formes/GameObject.h"
#include "Formes/ShadersParams.h"
#include "Formes/Sommet.h"
#include "Util/Horloge.h"
#include "Util/Logger.h"
#include "Util/RandomGenerateur.h"
#include "FileLoader.h"
#include "DirectInputManipulateur.h"
#include "GameState.h"
#include "Texture.h"
#include "Zone.h"
#include "ScenePhysique.h"

// Includes needed for some Components
#include "Texture.h"
#include "Material.h"

// Components
#include "Component/PhysicComponent.h"
#include "Component/MeshComponent.h"
#include "Component/PhysicTerrainComponent.h"
#include "Component/CamInputComponent.h"
#include "Component/CamFreeInputComponent.h"
#include "Component/CamThirdPersonInputComponent.h"
#include "Component/CamFirstPersonComponent.h"
#include "Component/InputPlayerComponent.h"
#include "Component/PhysicPlayerComponent.h"
#include "Component/PhysicTerrainComponent.h"
#include "Component/PhysicTriggerComponent.h"

// Camera, Lumiere
#include "Camera/FrustrumView.h"
#include "Camera/Lumiere.h"
#include "Camera/LumiereManager.h"
#include "Camera/Camera.h"

// HUD
#include "AfficheurTexte.h"
#include "AfficheurSprite.h"
#include "Component/InputMenuComponent.h"
#include "HUD.h"
#include "PanneauPE.h"
#include "MainMenu.h"
#include "OptionsMenu.h"
#include "EndMenu.h"

#include "Formes/Billboard.h"
#include "Formes/Chateau.h"
#include "Item.h"
#include "Skybox.h"
#include "Formes/Bloc.h"
#include "Formes/Terrain.h"
#include "GameManager.h"
#include "Vehicule.h"
#include "Scene.h"

// Rendu
#include "ShaderManager.h"
#include "RessourcesManager.h"

#define ALLOW_TO_INCLUDE_MOTEUR

#include "Util/Logger.h"
#include "Engine/MoteurWindows.h"

#undef ALLOW_TO_INCLUDE_MOTEUR

#pragma endregion
