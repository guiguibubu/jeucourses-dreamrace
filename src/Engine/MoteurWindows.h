#ifndef ALLOW_TO_INCLUDE_MOTEUR

#error Use Moteur****Module.h to include Moteur**** Header

#endif // !ALLOW_TO_INCLUDE_MOTEUR

#ifndef MOTEUR_WINDOWS_H
#define MOTEUR_WINDOWS_H

#define ALLOW_TO_INCLUDE_MOTEUR_TEMPLATE

#include "Moteur.h"

#undef ALLOW_TO_INCLUDE_MOTEUR_TEMPLATE

namespace PM3D
{
#define MAX_LOADSTRING 100

	class DispositifD3D11;
	class Horloge;

	class MoteurWindows final : public Moteur<MoteurWindows, DispositifD3D11>
	{
	public:
		void SetWindowsAppInstance(HINSTANCE hInstance);

	private:

	public:

		ATOM MyRegisterClass(HINSTANCE hInstance);
		bool InitAppInstance();
		int Show();

		// Les fonctions sp�cifiques
		virtual int InitialisationsSpecific() override;
		virtual bool RunSpecific() override;
		virtual int64_t GetTimeSpecific() const override;
		virtual float GetTimeIntervalsInSec(int64_t start, int64_t stop) const override;
		virtual DispositifD3D11* CreationDispositifSpecific(const CDS_MODE cdsMode) override;
		virtual void BeginRenderSceneSpecific() override;
		virtual void EndRenderSceneSpecific() override;
		virtual void CleanDepthStencil();

		virtual bool RenderScene() override;

		// Fonctions "Callback" -- Doivent �tre statiques
		static LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
		static INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

		HACCEL hAccelTable;						// handle Windows de la table des acc�l�rateurs
		static HINSTANCE hAppInstance;			// handle Windows de l'instance actuelle de l'application
		HWND hMainWnd;							// handle Windows de la fen�tre principale
		TCHAR szWindowClass[MAX_LOADSTRING];	// le nom de la classe de fen�tre principale

		Horloge m_Horloge;
	};

} // namespace PM3D

#endif // !MOTEUR_WINDOWS_H
