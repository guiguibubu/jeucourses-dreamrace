#pragma once

#include "Util/PlatformDetector.h"

#ifdef PM3D_WINDOWS

// Implementation Moteur Windows 
#include "Engine/MoteurWindowsModule.h"

#else

#error Moteur not implemented for this platform

#endif // 