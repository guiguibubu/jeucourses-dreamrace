#include "stdafx.h"

#include "dispositif.h"

namespace PM3D
{

	void Dispositif::Present()
	{
		PresentSpecific();
	};

} // namespace PM3D
