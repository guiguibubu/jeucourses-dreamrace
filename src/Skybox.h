#ifndef SKYBOX_H
#define SKYBOX_H

#include "Formes/GameObject.h"

#include <vector>
#include <memory>

namespace PM3D {

	class Orientation;
	class MeshComponent;
	class DispositifD3D11;

	class Skybox : public GameObject
	{
	public:
		MeshComponent* skyboxMesh;
		Skybox(const Orientation& _orientation, DispositifD3D11* _pDispositif);

		~Skybox()
		{
		}

		void Init(DispositifD3D11* _pDispositif);
		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr) override;
		void Update(const float tempsEcoule) override;
		// virtual void Update(const float tempsEcoule) override;
	private:

	};


}
#endif // SKYBOX_H