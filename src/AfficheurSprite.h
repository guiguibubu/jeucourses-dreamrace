#ifndef AFFICHEUR_SPRITE_H
#define AFFICHEUR_SPRITE_H

#include "Formes/GameObject.h"

using DirectX::XMFLOAT3;
using DirectX::XMFLOAT2;
using DirectX::XMMATRIX;

namespace PM3D
{

	class DispositifD3D11;

	class SommetSprite
	{
	public:
		SommetSprite() = default;
		SommetSprite(const XMFLOAT3& position, const XMFLOAT2& coordTex)
			: m_Position(position)
			, m_CoordTex(coordTex)
		{
		}

	public:
		static UINT numElements;
		static D3D11_INPUT_ELEMENT_DESC layout[];

		XMFLOAT3 m_Position;
		XMFLOAT2 m_CoordTex;
	};

	class AfficheurSprite : public GameObject
	{
	public:
		AfficheurSprite(DispositifD3D11* _pDispositif);
		virtual ~AfficheurSprite();
		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr) override;

		void AjouterSprite(const std::wstring& NomTexture, int _x, int _y, int _dx = 0, int _dy = 0);
		void AjouterPanneau(const std::string& NomTexture, const XMFLOAT3& _position,
			float _dx = 0.0f, float _dy = 0.0f);
		void AjouterSpriteTexte(ID3D11ShaderResourceView* pTexture, const DirectX::XMFLOAT2&);
		void MoveSprite(size_t index, int dx, int dy, const DirectX::XMFLOAT2&);

	private:
		class Sprite
		{
		public:
			ID3D11ShaderResourceView* pTextureD3D;

			XMMATRIX matPosDim;
			bool bPanneau;
			Sprite()
				: bPanneau(false)
				, pTextureD3D(nullptr)
			{
			}
		};

		class PanneauSprite : public Sprite
		{
		public:
			XMFLOAT3 position;
			XMFLOAT2 dimension;

			PanneauSprite()
			{
				bPanneau = true;
			}
		};

		static SommetSprite sommets[6];
		ID3D11Buffer* pVertexBuffer;
		DispositifD3D11* pDispositif;

		ID3D11Buffer* pConstantBuffer;
		ID3DX11Effect* pEffet;
		ID3DX11EffectTechnique* pTechnique;
		ID3DX11EffectPass* pPasse;
		ID3D11InputLayout* pVertexLayout;

		ID3D11SamplerState* pSampleState;

		// Tous nos sprites
		std::vector<std::unique_ptr<Sprite>> tabSprites;

		void InitEffet();
	};

} // namespace PM3D

#endif // !AFFICHEUR_SPRITE_H
