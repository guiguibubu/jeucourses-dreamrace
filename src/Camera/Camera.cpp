#include "stdafx.h"

#include "Util/toolsMath.h"
#include "Engine/DispositifD3D11.h"
#include "GameState.h"

#include "Engine/MoteurModule.h"

#include "Camera.h"

#include <math.h> 

namespace PM3D {

   void Camera::Init(DispositifD3D11* pDispositif)
   {
      frustrum = { DirectX::XM_PI / 4,  static_cast<float>(pDispositif->GetLargeur()) / static_cast<float>(pDispositif->GetHauteur()), 1.f, 20000.f };
      freeControl = std::make_unique<CamFreeInputComponent>( DirectX::XMVECTOR{ 0.0f, 10.0f, -10.0f, 1.0f }, DirectX::XMQuaternionIdentity() );
      levelControl = std::make_unique<CamThirdPersonInputComponent>();
		firstPersonControl= std::make_unique<CamFirstPersonComponent>();
      actualControl = levelControl.get();
      // Init matView
      actualControl->m_MatView = DirectX::XMMatrixLookAtLH(
         actualControl->orientation.getPosition(),
         actualControl->orientation.getPosition() + actualControl->orientation.getDirection(),
         actualControl->orientation.getUp());
   }

   void Camera::Update(DirectX::XMMATRIX& matViewProj, const float tempsEcoule) {
       HandleInput(tempsEcoule);
		 
       actualControl->m_MatView = DirectX::XMMatrixLookAtLH(
           actualControl->orientation.getPosition(),
           actualControl->orientation.getPosition() + actualControl->orientation.getDirection(),
           actualControl->orientation.getUp());

       frustrum.Update(matViewProj, actualControl->m_MatView);
   }

   void Camera::HandleInput(const float tempsEcoule) {
      auto& GestionnaireDeSaisie = MoteurWindows::GetInstance().GetGestionnaireDeSaisie();

      if (GestionnaireDeSaisie.ToucheAppuyee(DIK_1))
      {
          actualControl = levelControl.get();
      }
      else if (GestionnaireDeSaisie.ToucheAppuyee(DIK_2))
      {
          actualControl = freeControl.get();
      }
		else if (GestionnaireDeSaisie.ToucheAppuyee(DIK_3))
		{
			actualControl = firstPersonControl.get();
		}

      actualControl->Handle(tempsEcoule);	
   }

   const FrustrumView& Camera::getFrustrumView() const {
       return frustrum;
   }

   Orientation& Camera::getOrientation() {
       return actualControl->orientation;
   }

   const DirectX::XMMATRIX& Camera::GetMatView() const {
       return actualControl->m_MatView;
   }

   bool Camera::isLevel() const {
       return actualControl->inputType == LEVEL;
   }

	bool Camera::isFirstPerson() const {
		return actualControl->inputType == FPS;
	}
}