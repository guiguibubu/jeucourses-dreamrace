#ifndef CAMERA_H
#define CAMERA_H

namespace PM3D
{
	class Orientation;
	class DispositifD3D11;
	class FrustrumView;
	class CamInputComponent;
	class CamFreeInputComponent;
	class CamThirdPersonInputComponent;
	class CamFirstPersonComponent;

	class Camera
	{
	private:
		FrustrumView frustrum;
		std::unique_ptr<CamFreeInputComponent> freeControl;
		std::unique_ptr<CamThirdPersonInputComponent> levelControl;
		std::unique_ptr<CamFirstPersonComponent> firstPersonControl;
		CamInputComponent* actualControl;

		void HandleInput(const float);
	public:
		Camera() = default;
		~Camera() = default;

		void Init(DispositifD3D11* pDispositif);
		void Update(DirectX::XMMATRIX& matViewProj, const float tempsEcoule);

		const FrustrumView& getFrustrumView() const;
		Orientation& getOrientation();
		const DirectX::XMMATRIX& GetMatView() const;

		bool isLevel() const;
		bool isFirstPerson() const;
	};
}
#endif // !CAMERA_H