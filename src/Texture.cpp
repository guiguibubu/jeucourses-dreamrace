#include "stdafx.h"

#include "Util/util.h"
#include "Resource.h"
#include "Engine/DispositifD3D11.h"

#include "DDSTextureLoader.h"

#include "Texture.h"

namespace PM3D
{

	Texture::~Texture()
	{
		// DXRelacher(ressource);
	}

	Texture::Texture(const std::wstring& filename, DispositifD3D11* pDispositif)
		: m_Filename(filename)
		, ressource(nullptr)
	{
		ID3D11Device* pDevice = pDispositif->GetD3DDevice();

		// Charger la texture en ressource
		DXEssayer(DirectX::CreateDDSTextureFromFile(pDevice,
			m_Filename.c_str(),
			nullptr,
			&ressource), DXE_FICHIERTEXTUREINTROUVABLE);
	}

} // namespace PM3D
