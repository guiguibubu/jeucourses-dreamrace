#ifndef GESTIONNAIRE_TEXTURES_H
#define GESTIONNAIRE_TEXTURES_H

namespace PM3D
{
	class Texture;
	class MeshComponent;
	class ParametresChargement;

	enum ModelType;

	class RessourcesManager
	{
		struct ModelInfo
		{
			ModelType type;
			ParametresChargement param;

			ModelInfo(const ModelType type, const ParametresChargement& param)
				:type{ type }, param{ param }
			{}
			~ModelInfo() = default;
		};

		struct Model
		{
			ModelType type;
			MeshComponent* mesh;

			Model(const ModelType type, MeshComponent* mesh)
				:type{ type }, mesh{ mesh }
			{}
			~Model() = default;
		};

		static const std::vector<std::wstring> TexturesToLoad;
		static const std::vector<ModelInfo> ModelsToLoad;
	public:
		void LoadAll(DispositifD3D11*);
		Texture* const GetTexture(const std::wstring&);
		MeshComponent* const GetModelMesh(const ModelType);
	private:
		std::vector<Model> loadedModels;
		std::vector<std::unique_ptr<Texture>> loadedTextures;
	};

} // namespace PM3D
#endif // !GESTIONNAIRE_TEXTURES_H