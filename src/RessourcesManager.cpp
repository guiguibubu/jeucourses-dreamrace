#include "stdafx.h"

#include "Engine/DispositifD3D11.h"
#include "GameState.h"

#include "FileLoader.h"
#include "AssimpLoader.h"
#include "Formes/Orientation.h"
#include "Texture.h"
#include "Formes/ShadersParams.h"
#include "Component/MeshComponent.h"

#include "RessourcesManager.h"

#include <algorithm>

namespace PM3D
{
    const std::vector<std::wstring> RessourcesManager::TexturesToLoad = {
        L"../ressources/textures/stone.dds",
        L"../ressources/textures/grass.dds",
        L"../ressources/textures/green_grass.dds",
        L"../ressources/assets/cube/bear.dds",
        L"../ressources/textures/brick_pavement.dds",
        L"../ressources/textures/brick.dds",
        L"../ressources/textures/MainMenu.dds",
        L"../ressources/textures/stoneNM.dds",
        L"../ressources/textures/grassNM.dds",
        L"../ressources/textures/brickNM.dds",
        L"../ressources/textures/Star.dds",
        L"../ressources/assets/skybox/skybox.dds",
        L"../ressources/assets/mysterybox/rewards.dds"
    };

    const std::vector<RessourcesManager::ModelInfo> RessourcesManager::ModelsToLoad = {
        { ModelType::CLOUD, { std::string{"../ressources/assets/cloud/"}, "cloud.obj" } },
        { ModelType::CUBE_TRI, { std::string{"../ressources/assets/cube/"}, "cubeDos.obj" } },
        { ModelType::TREE_1,{ std::string{ "../ressources/assets/tree/" }, "Lowpoly_tree_sample.obj" } },
        { ModelType::TREE_2,{ std::string{ "../ressources/assets/tree/" }, "Lowpoly_tree_sample2.obj" } },
        { ModelType::TREE_3,{ std::string{ "../ressources/assets/tree/" }, "Lowpoly_tree_sample3.obj" } },
        { ModelType::BEAR, { std::string{"../ressources/assets/bear/choupibear/"}, "choupibear.obj" }},
        { ModelType::SANDWATCH, { std::string{"../ressources/assets/sandwatch/sandwatch1/"}, "Sand_Watch_tri_back.obj" }},
        { ModelType::SKYBOX,{ std::string{ "../ressources/assets/skybox/" }, "skybox.obj" } },
        { ModelType::MYSTERYBOX,{ std::string{ "../ressources/assets/mysterybox/" }, "mysterybox.obj" } }
    };

    void RessourcesManager::LoadAll(DispositifD3D11* _pDispositif)
    {
        for (const auto& textureName : TexturesToLoad)
            loadedTextures.push_back(std::make_unique<Texture>(textureName, _pDispositif));

        ChargeurAssimp chargeur;
        for (auto& modelInfo : ModelsToLoad)
        {
            chargeur.Chargement(modelInfo.param);
            loadedModels.emplace_back(modelInfo.type, new MeshComponent(chargeur));
        }
    }

    Texture* const RessourcesManager::GetTexture(const std::wstring& filename)
    {
        auto iter = std::find_if(loadedTextures.begin(), loadedTextures.end(), [&filename](std::unique_ptr<Texture>& t) {
            return t->GetFilename() == filename;
        });

        assert(iter != loadedTextures.end() && "Add your texture to TexturesToInit, we load them all at once at the beginning");
        return iter->get();
    }

    MeshComponent* const RessourcesManager::GetModelMesh(const ModelType type)
    {
        auto iter = std::find_if(loadedModels.begin(), loadedModels.end(), [&type](const Model& m) {
            return m.type == type;
        });

        assert(iter != loadedModels.end() && "Add your model to ModelsToInit, we load them all at once at the beginning");
        return iter->mesh;
    }
} // namespace PM3D
