#ifndef VEHICULE_H
#define VEHICULE_H

#include "Formes/GameObject.h"

#include <memory>
#include <vector>
#include <chrono>

namespace PM3D
{
	class MeshComponent;
	class Terrain;
	class ScenePhysique;
	class Orientation;
	class PhysicPlayerComponent;
	class InputPlayerComponent;
	class Billboard;

	class Vehicule : public GameObject
	{
	public:
		MeshComponent* mesh;
		std::unique_ptr<PhysicPlayerComponent> physic;
		std::unique_ptr<InputPlayerComponent> input;
		std::vector<std::unique_ptr<Billboard>> stars;
		std::chrono::milliseconds dureeDisableMove{ 0 };
		std::chrono::time_point<std::chrono::steady_clock> lastTimeDisableMove = std::chrono::high_resolution_clock::now();

		Vehicule(const Orientation& _orientation, const float _dx, const float _dy, const float _dz, DispositifD3D11* _pDispositif, ScenePhysique& scenePhysique);
		virtual ~Vehicule() = default;

		bool canMove() const noexcept; // Permet de savoir si le v�hicule peut bouger ou non
		void disableMoveFor(std::chrono::milliseconds duree); // Permet de d�sactiver le mouvement du joueur pendant une certain p�riode

		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr) override;
		virtual void DrawShadow(const XMMATRIX matViewProjLight);
		virtual void Update(const float tempsEcoule, const Terrain& terrain, ScenePhysique& sp);
	};

} // namespace PM3D

#endif // !VEHICULE_H
