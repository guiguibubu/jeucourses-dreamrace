#include "stdafx.h"

#include "Util/toolsMath.h"

#include "Orientation.h"

#include "GameObject.h"

namespace PM3D {

	GameObject::GameObject(const Orientation& _orientation) :
		orientation{ _orientation }, matWorld{ DirectX::XMMatrixScalingFromVector(convertIntoVector(_orientation.getScale())) }
	{}

	void GameObject::calculMatriceWorld()
	{
		matWorld = orientation.calculMatriceWorld();
	}
}