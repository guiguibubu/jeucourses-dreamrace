#ifndef SIMPLE_GAME_OBJECT_H
#define SIMPLE_GAME_OBJECT_H

#include "GameObject.h"

namespace PM3D
{
	class Orientation;
	class DispositifD3D11;
	class MeshComponent;
	class ScenePhysique;

	enum ModelType;

	class SimpleGameObject : public GameObject
	{
	public:
		SimpleGameObject(Orientation& orientation, ModelType type, DispositifD3D11* _pDispositif, ScenePhysique& scenePhysique);
		MeshComponent* mesh;
		physx::PxShape* shape = NULL;
		physx::PxRigidDynamic* rigidbody = NULL;
		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView) override;
		void DrawShadow(const DirectX::XMMATRIX matViewProjLight);
	};

} // namespace PM3D
#endif // SIMPLE_GAME_OBJECT_H