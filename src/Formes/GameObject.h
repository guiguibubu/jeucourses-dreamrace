#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "stdafx.h"

namespace PM3D
{
	class Orientation;
	class GameObject
	{
	public:
		DirectX::XMMATRIX matWorld;
		Orientation orientation;

		GameObject() = default;
		GameObject(const Orientation& _orientation);
		virtual ~GameObject() = default;

		void calculMatriceWorld();

		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr) = 0;
		virtual void DrawShadow(const DirectX::XMMATRIX matViewProjLight) {};
		virtual void Update(const float) {};
	};
}
#endif // !GAME_OBJECT_H


