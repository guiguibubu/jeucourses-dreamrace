#ifndef CHATEAU_H
#define CHATEAU_H

#include <vector>

using std::vector;

namespace PM3D
{
	class DispositifD3D11;
	class ScenePhysique;
	class Bloc;

	class Chateau {
		vector<Bloc> murs;
		vector<Bloc> toitsTunnel;
		float largeur = 25.f;
		float hauteur = 125.f;
	public:
		Chateau(DispositifD3D11* pDispositif, ScenePhysique& scenePhysique);

		void Draw();
		void DrawShadow(const XMMATRIX matViewProjLight);

		bool isInTunnel(float x, float y) const;
		bool isInChateau(float x, float z, float dx, float dz) const;
	};

} // namespace PM3D

#endif // !Chateau
