#ifndef BILLBOARD_H
#define BILLBOARD_H

#include "Formes/GameObject.h"

#include <memory>

namespace PM3D
{
	class Orientation;
	class BillboardMeshComponent;
	class DispositifD3D11;

	class Billboard : public GameObject
	{
		const float neededAngle = DirectX::XM_PIDIV4;
	public:

		std::unique_ptr<BillboardMeshComponent> mesh;
		DirectX::XMVECTOR test;
		DirectX::XMVECTOR offset;

		Billboard(DispositifD3D11* _pDispositif, const Orientation& _orientation, const DirectX::XMVECTOR& offset, const std::wstring& filename);
		virtual ~Billboard() = default;

		virtual void Draw(ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr) override;
		virtual void Update(const float);
	};
}
#endif // !C_GAME_OBJECT_H


