#ifndef SOMMET_H
#define SOMMET_H

namespace PM3D
{
	class Sommet
	{
	public:
		Sommet() = default;
		Sommet(const DirectX::XMFLOAT3& position);
		Sommet(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& normal);
		Sommet(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& normal, const DirectX::XMFLOAT2& texCoord);

		static UINT numElements;
		static D3D11_INPUT_ELEMENT_DESC layout[];

		DirectX::XMFLOAT3 m_Position;
		DirectX::XMFLOAT3 m_Normal;
		DirectX::XMFLOAT2 m_TexCoord;
		DirectX::XMFLOAT3 m_Tangent;
		DirectX::XMFLOAT3 m_Bitangent;
	};

} // namespace PM3D
#endif // SOMMET_H