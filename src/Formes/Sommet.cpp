#include "stdafx.h"

#include "Sommet.h"

namespace PM3D {

	// Definir l'organisation de notre sommet
	D3D11_INPUT_ELEMENT_DESC Sommet::layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BITANGENT", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	UINT Sommet::numElements = ARRAYSIZE(Sommet::layout);

	Sommet::Sommet(const DirectX::XMFLOAT3& position)
		: m_Position(position)
	{}

	Sommet::Sommet(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& normal, const DirectX::XMFLOAT2& texCoord)
		: m_Position(position)
		, m_Normal(normal)
		, m_TexCoord(texCoord)
	{}

	Sommet::Sommet(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& normal)
		: m_Position(position)
		, m_Normal(normal)
	{}
}