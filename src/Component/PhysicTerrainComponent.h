#ifndef PHYSIC_TERRAIN__COMPONENT_H
#define PHYSIC_TERRAIN__COMPONENT_H

#include "PhysicComponent.h"

namespace PM3D
{
    class GameObject;
    class ScenePhysique;

    struct PhysicTerrainComponent : PhysicComponent
    {
        PhysicTerrainComponent(GameObject* obj, ScenePhysique& scenePhysique);
        ~PhysicTerrainComponent() = default;
    };
}
#endif