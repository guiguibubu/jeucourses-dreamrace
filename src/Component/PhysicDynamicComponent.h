#ifndef PHYSIC_DYNAMIC_COMPONENT_H
#define PHYSIC_DYNAMIC_COMPONENT_H

#include "PhysicComponent.h"

#include <cmath>

namespace PM3D
{
	class Orientation;
	class GameObject;
	class ScenePhysique;
	class Terrain;

	struct PhysicDynamicComponent : PhysicComponent
	{
		DirectX::XMVECTOR vitesseLineaire{ 0.0f, 0.0f, 0.0f };
		DirectX::XMVECTOR vitesseAngulaire{ 0.0f, 0.0f, 0.0f };

		void UpdateSimulation(Orientation orientation, const Terrain& terrain, ScenePhysique& sp);
		void UpdateFromSimulation(GameObject* obj);
		int GetVitesse();

		PhysicDynamicComponent() = default;
		~PhysicDynamicComponent() = default;
	};
}
#endif