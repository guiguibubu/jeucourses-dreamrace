#include "stdafx.h"

#include "Formes/Orientation.h"

#include "CamInputComponent.h"

namespace PM3D
{
	CamInputComponent::CamInputComponent(DirectX::XMVECTOR&& position, DirectX::XMVECTOR&& quaternion, ControlInputType controlType) :
		orientation{ std::move(position), std::move(quaternion) }, inputType{ controlType }
	{}
}