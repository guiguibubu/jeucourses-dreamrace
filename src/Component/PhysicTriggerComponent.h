#ifndef PHYSIC_TRIGGER_COMPONENT_H
#define PHYSIC_TRIGGER_COMPONENT_H

#include "PhysicComponent.h"

namespace PM3D
{
	class ScenePhysique;
	class GameObject;

	struct PhysicTriggerComponent : PhysicComponent
	{
		PhysicTriggerComponent(GameObject * obj, ScenePhysique & scenePhysique, int index);

		~PhysicTriggerComponent() = default;
	};

}
#endif