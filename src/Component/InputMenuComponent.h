#ifndef INPUT_MENU_COMPONENT_H
#define INPUT_MENU_COMPONENT_H

namespace PM3D
{
    class AfficheurSprite;

    struct InputMenuComponent
    {
        virtual void Handle(const float, AfficheurSprite*);

        InputMenuComponent(std::vector<DirectX::XMFLOAT2>&&);
        virtual ~InputMenuComponent() = default;

    private:
        bool currentPressedState = false;

        unsigned int currentIndex;
        std::vector<DirectX::XMFLOAT2> position;
    };
}
#endif