#ifndef INPUT_COMPONENT_H
#define INPUT_COMPONENT_H

namespace PM3D
{
    class GameObject;
    struct InputComponent
    {
        virtual void Handle(const float) = 0;

    protected:
        InputComponent() = default;
        virtual ~InputComponent() = default;
    };

    struct InputPhysicComponent
    {
        virtual void Handle(const float, GameObject*) = 0;

    protected:
        InputPhysicComponent() = default;
        virtual ~InputPhysicComponent() = default;
    };
}
#endif