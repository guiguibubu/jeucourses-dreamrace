#ifndef BILLBOARD_MESH_COMPONENT_H
#define BILLBOARD_MESH_COMPONENT_H

#include <vector>

namespace PM3D
{
    class DispositifD3D11;
    class ShadersParams;
    class Material;
    class Sommet;

    class BillboardMeshComponent
    {
    public:
        static Sommet sommets[6];
        LPCWSTR shaderFilename;

        void Draw(const DirectX::XMMATRIX& matriceWorld, ID3D11ShaderResourceView* pDepthShaderResourceView = nullptr);
        void InitEffet();
        void InitBuffer();

        BillboardMeshComponent(DispositifD3D11* _pDispositif, const std::wstring& filename);
        virtual ~BillboardMeshComponent() = default;

        DirectX::XMVECTOR bottomVector;

    protected:
        Material material;
        DispositifD3D11* pDispositif;		// On prend en note le dispositif
        ID3D11Buffer* pVertexBuffer = nullptr;
        ID3D11Buffer* pIndexBuffer = nullptr;
        ID3D11SamplerState* pSampleState = nullptr;
        ID3D11Buffer* pConstantBuffer = nullptr;
        ID3DX11Effect* pEffet = nullptr;
        ID3DX11EffectTechnique* pTechnique = nullptr;
        ID3DX11EffectPass* pPasse = nullptr;
        ID3D11InputLayout* pVertexLayout = nullptr;
        ShadersParams sp;                 // Les parametres des shaders utilis�s dans draw
    };
}
#endif