#ifndef PHYSIC_COMPONENT_H
#define PHYSIC_COMPONENT_H

namespace PM3D
{
    class Terrain;
    class ScenePhysique;

    struct PhysicComponent
    {
        physx::PxShape* shape = NULL;
        physx::PxRigidDynamic* rigidbody = NULL;
        DirectX::XMVECTOR boundingBox;

        PhysicComponent() = default;
        ~PhysicComponent() = default;
    };
}
#endif