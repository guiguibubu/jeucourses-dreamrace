#ifndef END_MENU_H
#define END_MENU_H

#include <memory>

namespace PM3D {

	class AfficheurSprite;
	class AfficheurTexte;
	class DispositifD3D11;

	enum GameResult;

	using ZoneTextPtr = std::unique_ptr<AfficheurTexte>;

	class EndMenu
	{
		std::unique_ptr<Gdiplus::Font> pPolice;
		ZoneTextPtr result;
		ZoneTextPtr score;
		std::wstring lastTime;

	public:
		EndMenu() = default;
		void Init(DispositifD3D11*, std::unique_ptr<AfficheurSprite>&);
		~EndMenu() = default;

		void Update(const float, GameResult, std::chrono::milliseconds remainingTime);
	};

} // namespace PM3D


#endif